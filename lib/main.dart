import 'package:flutter/material.dart';
import 'package:restaurant_list/screens/categories_window.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.amber,
        hintColor: Colors.red,
        textTheme: ThemeData.light().textTheme.copyWith(
            headline1: TextStyle(
              fontSize: 22,
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontFamily: "SecularOne",
            ),
            caption: TextStyle(
              fontSize: 20,
              color: Colors.black,
              fontWeight: FontWeight.bold,
            )),
      ),
      home: CategoriesWindow(),
    );
  }
}
