
class Category{
  final String id;
  final String name;
  final String imageUrl;

  Category(this.id, this.name, this.imageUrl);
}

class Categories{
  List<Category> _things = [
    Category("c1", "Fast Food", "assets/images/fast_food.jpg"),
    Category("c2", "Italian Foods", "assets/images/itali.jpg"),
    Category("c3", "National Foods", "assets/images/natio.jpg"),
    Category("c4", "France Foods", "assets/images/france.jpg"),
    Category("c5", "Drinks", "assets/images/drinks.jpg"),
    Category("c6", "Salads", "assets/images/salads.jpg"),
    Category("c7", "American Foods", "assets/images/america.jpg"),
    Category("c8", "Dessert", "assets/images/dessert.jpg"),
  ];

  List<Category> get things {
    return _things;
  }

}