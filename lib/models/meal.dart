class Meal {
  final String mealId;
  final String categoryID;
  final String mealName;
  final String price;
  final String imageMealUrl;
  final String prepTime;
  bool favourite = false;
  final String rateMeal;
  final List<String> retsept;

  Meal({
    required this.mealId,
    required this.categoryID,
    required this.mealName,
    required this.price,
    required this.imageMealUrl,
    required this.prepTime,
    required this.rateMeal,
    required this.retsept,
  });

  void favOrNotFav(){
    favourite = !favourite;
  }

}

class Meals {
  List<Meal> _meals = [
    Meal(
      mealId: "m1",
      categoryID: "c1",
      mealName: "Lavash",
      price: "\$22.2",
      imageMealUrl: "assets/images/lavash.jpg",
      prepTime: "20 min",
      rateMeal: "Great",
      retsept: ["Go'sht", "Pomidor", "Bodring", "Sous"],
    ),
    Meal(
      mealId: "m2",
      categoryID: "c1",
      mealName: "Donar",
      price: "\$29.99",
      imageMealUrl: "assets/images/donar.jpg",
      prepTime: "25 min",
      rateMeal: "Nice",
      retsept: ["Go'sht", "Pomidor", "Bodring"],
    ),
    Meal(
      mealId: "m3",
      categoryID: "c1",
      mealName: "Barbekyu",
      price: "\$25.99",
      imageMealUrl: "assets/images/barbekyu.jpg",
      prepTime: "15 min",
      rateMeal: "Ajoyib",
      retsept: ["Go'sht", "Pomidor", "Bodring"],
    ),
    Meal(
      mealId: "m4",
      categoryID: "c1",
      mealName: "Clab Sendvich",
      price: "\$20.99",
      imageMealUrl: "assets/images/klab.jpg",
      prepTime: "30 min",
      rateMeal: "Mazali",
      retsept: ["Go'sht", "Pomidor", "Bodring"],
    ),
    Meal(
      mealId: "m5",
      categoryID: "c3",
      mealName: "Osh",
      price: "\$50.99",
      imageMealUrl: "assets/images/osh.jpg",
      prepTime: "50 min",
      rateMeal: "Shirin",
      retsept: ["Go'sht", "Sabzi", "Guruch", "Piyoz", "Yog'"],
    ),
    Meal(
      mealId: "m6",
      categoryID: "c3",
      mealName: "Shashlik",
      price: "\$30.99",
      imageMealUrl: "assets/images/shashlik.jpg",
      prepTime: "25 min",
      rateMeal: "Chotki",
      retsept: ["Qiyma", "Piyoz"],
    ),
    Meal(
      mealId: "m7",
      categoryID: "c6",
      mealName: "Bahorgi",
      price: "\$12.99",
      imageMealUrl: "assets/images/bahorgi_salat.jpg",
      prepTime: "15 min",
      rateMeal: "Yaxshiroq",
      retsept: ["Bodring", "Pomidor"],
    ),
    Meal(
      mealId: "m8",
      categoryID: "c6",
      mealName: "Aliviya",
      price: "\$5.99",
      imageMealUrl: "assets/images/alivia.jpg",
      prepTime: "12 min",
      rateMeal: "Yaxshi",
      retsept: ["Tuxum", "Lavlagi"],
    ),
    Meal(
      mealId: "m9",
      categoryID: "c6",
      mealName: "Baqlajon",
      price: "\$5.99",
      imageMealUrl: "assets/images/baqlajon.jpeg",
      prepTime: "10 min",
      rateMeal: "Yomon",
      retsept: ["Baqlajon", "Bolgar"],
    ),
  ];


  List<Meal> narsalarKategoriyaBoyicha(String categoryID) {
    return _meals.where((meal) => meal.categoryID == categoryID).toList();
  }

}
