import 'package:flutter/material.dart';
import 'package:restaurant_list/models/meal.dart';

class MealRetseptScreen extends StatelessWidget {
  final Meal meal;
  MealRetseptScreen(this.meal);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[600],
      appBar: AppBar(
        title: Text(
          meal.mealName,
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: Column(
        children: [
          Image.asset(meal.imageMealUrl, fit: BoxFit.cover),
          Padding(
            padding: const EdgeInsets.only(left: 29, right: 29, top: 17),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  meal.rateMeal,
                  style: TextStyle(
                    fontSize: 35,
                    color: Colors.white,
                    fontFamily: "SecularOne",
                    fontWeight: FontWeight.bold,
                  ),
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(17),
                  child: Container(
                    height: 56,
                    width: 100,
                    color: Colors.yellow,
                    child: Center(
                      child: Text(
                        meal.price,
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontFamily: "SecularOne",
                          fontSize: 25,
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            child: Text(
              "Tarkibi:",
              style: TextStyle(
                color: Colors.white,
                fontSize: 22,
                fontFamily: "SecularOne",
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: Card(
              color: Colors.yellow[600],
              margin: EdgeInsets.all(0),
              child: ListView.separated(
                padding: EdgeInsets.all(15),
                itemCount: meal.retsept.length,
                itemBuilder: (ctx, i) {
                  return Text(
                    meal.retsept[i],
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontFamily: "SecularOne",
                      fontWeight: FontWeight.bold,
                    ),
                  );
                },
                separatorBuilder: (ctx, i) {
                  return Divider(
                    thickness: 1,
                    color: Colors.yellow,
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
