import 'package:flutter/material.dart';
import 'package:restaurant_list/models/meal.dart';
import 'package:restaurant_list/widgets/meal_item.dart';

class CategoryMealList extends StatefulWidget {
  final List<Meal> ovqatlar;
  final String categoryName;
  final Function sevimliORNotFav;

  CategoryMealList(this.ovqatlar, this.categoryName, this.sevimliORNotFav);

  @override
  _CategoryMealListState createState() => _CategoryMealListState();
}

class _CategoryMealListState extends State<CategoryMealList> {

  // @override
  // void initState() {
  //   ovqatlar = Meals().narsalarKategoriyaBoyicha(widget.categoryID);
  //   super.initState();
  // }

 

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[600],
      appBar: AppBar(
        title: Text(
          widget.categoryName,
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: widget.ovqatlar.length > 0
          ? ListView.builder(
              padding: EdgeInsets.all(12),
              itemBuilder: (ctx, i) {
                return MealItem(
                  widget.ovqatlar[i], widget.sevimliORNotFav,
                  // ovqatlar[i].mealId,
                  // ovqatlar[i].mealName,
                  // ovqatlar[i].price,
                  // ovqatlar[i].imageMealUrl,
                  // ovqatlar[i].prepTime,
                  // ovqatlar[i].favourite,
                  // ovqatlar[i].rateMeal,
                  // ovqatlar[i].retsept,
                );
              },
              itemCount: widget.ovqatlar.length,
            )
          : Center(
              child: Text("Not Found"),
            ),
    );
  }
}
