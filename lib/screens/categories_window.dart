import 'package:flutter/material.dart';
import 'package:restaurant_list/models/category.dart';
import 'package:restaurant_list/models/meal.dart';
import 'package:restaurant_list/widgets/category_item.dart';

class CategoriesWindow extends StatelessWidget {
  final categories = Categories().things;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.yellow[600],
      appBar: AppBar(
        backgroundColor: Colors.amberAccent,
        title: Text(
          "Menu",
          style: Theme.of(context).textTheme.headline1,
        ),
      ),
      body: GridView(
        padding: EdgeInsets.only(top: 20, left: 17, right: 17),
        gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
          maxCrossAxisExtent: 200,
          childAspectRatio: 3 / 2,
          crossAxisSpacing: 20,
          mainAxisSpacing: 20,
        ),
        children: categories.map((category){
          final ovqatlar = Meals().narsalarKategoriyaBoyicha(category.id);
          return CategoryItem(category.id, category.name, category.imageUrl, ovqatlar);
        }).toList(),
      ),
    );
  }
}
