import 'package:flutter/material.dart';
import 'package:restaurant_list/models/meal.dart';
import 'package:restaurant_list/screens/meal_retsept_screen.dart';

class MealItem extends StatefulWidget {
  // final String mealId;
  // final String mealName;
  // final String price;
  // final String imageMealUrl;
  // final String prepTime;
  // bool favourite;
  // final String rateMeal;
  // final List<String> retsept;

  // MealItem(
  //   this.mealId,
  //   this.mealName,
  //   this.price,
  //   this.imageMealUrl,
  //   this.prepTime,
  //   this.favourite,
  //   this.rateMeal,
  //   this.retsept,
  // );

  final Meal meal;
  final Function sevimliOrNot;
  MealItem(this.meal, this.sevimliOrNot);

  @override
  _MealItemState createState() => _MealItemState();
}

class _MealItemState extends State<MealItem> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.all(15.0),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (ctx) {
                    return MealRetseptScreen(widget.meal);
                  },
                ),
              );
            },
            child: Card(
              color: Colors.amber,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(60),
                  topRight: Radius.circular(60),
                  bottomRight: Radius.circular(27),
                  bottomLeft: Radius.circular(27),
                ),
              ),
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  Container(
                    width: double.infinity,
                    height: 250,
                    child: Stack(
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(60),
                            image: DecorationImage(
                              image: AssetImage(widget.meal.imageMealUrl),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: ClipRRect(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(60),
                              bottomRight: Radius.circular(60),
                            ),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              width: 270,
                              height: 65,
                              color: Colors.black.withOpacity(0.5),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 40),
                                child: Text(
                                  widget.meal.mealName,
                                  style: Theme.of(context).textTheme.headline1,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(13.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          widget.meal.prepTime,
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Container(
                          width: 2,
                          height: 24,
                          color: Colors.black38,
                        ),
                        Text(
                          widget.meal.price,
                          style: Theme.of(context).textTheme.caption,
                        ),
                        Container(
                          width: 2,
                          height: 24,
                          color: Colors.black38,
                        ),
                        IconButton(
                          onPressed: () {
                            setState(() {
                              widget.sevimliOrNot(widget.meal.mealId);
                            });
                          },
                          icon: Icon(
                            widget.meal.favourite
                                ? Icons.favorite_rounded
                                : Icons.favorite_outline_outlined,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
