import 'package:flutter/material.dart';
import 'package:restaurant_list/models/meal.dart';
import 'package:restaurant_list/screens/category_meal_list.dart';

class CategoryItem extends StatefulWidget {
  final String id;
  final String name;
  final String imageUrl;
  final List<Meal> ovqatlar;
  CategoryItem(this.id, this.name, this.imageUrl, this.ovqatlar);
  

  @override
  _CategoryItemState createState() => _CategoryItemState();
  

}
class _CategoryItemState extends State<CategoryItem> {
  void sevimliOrNot(String id) {
    setState(() {
      widget.ovqatlar.firstWhere((meal) => meal.mealId == id).favOrNotFav();
    });
   }
  
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(
          MaterialPageRoute(
            builder: (ctx) {
              return CategoryMealList(widget.ovqatlar, widget.name, sevimliOrNot);
            },
          ),
        );
      },
      child: Container(
        width: 200,
        height: 160,
        child: Stack(
          children: [
            Container(
              decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    offset: Offset(4, 12),
                    blurRadius: 12,
                    color: Colors.black,
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.circular(29),
                image: DecorationImage(
                  image: AssetImage(widget.imageUrl),
                  fit: BoxFit.fill,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, top: 14),
              child: Text(
                widget.name,
                style: Theme.of(context).textTheme.headline1,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
